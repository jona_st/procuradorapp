package pe.edu.upc.procuradorapp;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import pe.edu.upc.procuradorapp.rest.RestApi;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Constantes {

    public static RestApi createRestApi(){
        OkHttpClient.Builder builder= new OkHttpClient.Builder();


        HttpLoggingInterceptor loing = new HttpLoggingInterceptor();
        loing.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(loing);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.43.71:8080")
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(RestApi.class);
    }
}
