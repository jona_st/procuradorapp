package pe.edu.upc.procuradorapp.beans;

import java.io.Serializable;

public class ProcuradorBean implements Serializable{

    private Integer coProcurador;

    public Integer getCoProcurador() {
        return coProcurador;
    }

    public void setCoProcurador(Integer coProcurador) {
        this.coProcurador = coProcurador;
    }
}
