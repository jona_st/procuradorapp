package pe.edu.upc.procuradorapp.beans;

import java.io.Serializable;

public class SiniestroBean implements Serializable {

private Integer coSiniestro;
private String feCreacion;
private String feAtencion;
private String txCordenadas;
private String flEstado;
private String flZona;
private ProcuradorBean coProcurador;

    public ProcuradorBean getCoProcurador() {
        return coProcurador;
    }

    public void setCoProcurador(ProcuradorBean coProcurador) {
        this.coProcurador = coProcurador;
    }

    public Integer getCoSiniestro() {
        return coSiniestro;
    }

    public void setCoSiniestro(Integer coSiniestro) {
        this.coSiniestro = coSiniestro;
    }

    public String getFeCreacion() {
        return feCreacion;
    }

    public void setFeCreacion(String feCreacion) {
        this.feCreacion = feCreacion;
    }

    public String getFeAtencion() {
        return feAtencion;
    }

    public void setFeAtencion(String feAtencion) {
        this.feAtencion = feAtencion;
    }



    public String getFlEstado() {
        return flEstado;
    }

    public void setFlEstado(String flEstado) {
        this.flEstado = flEstado;
    }

    public String getTxCordenadas() {
        return txCordenadas;
    }

    public void setTxCordenadas(String txCordenadas) {
        this.txCordenadas = txCordenadas;
    }

    public String getFlZona() {
        return flZona;
    }

    public void setFlZona(String flZona) {
        this.flZona = flZona;
    }

    public String getFlPresenciaPolicial() {
        return flPresenciaPolicial;
    }

    public void setFlPresenciaPolicial(String flPresenciaPolicial) {
        this.flPresenciaPolicial = flPresenciaPolicial;
    }

    public String getFlCamara() {
        return flCamara;
    }

    public void setFlCamara(String flCamara) {
        this.flCamara = flCamara;
    }

    public String getFlTipoPersona() {
        return flTipoPersona;
    }

    public void setFlTipoPersona(String flTipoPersona) {
        this.flTipoPersona = flTipoPersona;
    }

    public String getFlDenunciaPolicial() {
        return flDenunciaPolicial;
    }

    public void setFlDenunciaPolicial(String flDenunciaPolicial) {
        this.flDenunciaPolicial = flDenunciaPolicial;
    }

    public String getNuDenunciaPolicial() {
        return nuDenunciaPolicial;
    }

    public void setNuDenunciaPolicial(String nuDenunciaPolicial) {
        this.nuDenunciaPolicial = nuDenunciaPolicial;
    }

    public String getFlConsecuencia() {
        return flConsecuencia;
    }

    public void setFlConsecuencia(String flConsecuencia) {
        this.flConsecuencia = flConsecuencia;
    }

    private String flPresenciaPolicial;
private String flCamara;
private String flTipoPersona;
private String flDenunciaPolicial;
private String nuDenunciaPolicial;
private String flConsecuencia;


    public SiniestroBean(Integer coSiniestro, String feCreacion) {
        this.coSiniestro = coSiniestro;
        this.feCreacion = feCreacion;
    }

    public SiniestroBean() {
    }
}
