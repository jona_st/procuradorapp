package pe.edu.upc.procuradorapp.beans;

public class UserBean {
   
    private Integer coUsuario;
    private String noUsuario;
    private String txPassword;
    private String flActivo;


    public Integer getCoUsuario() {
        return coUsuario;
    }

    public void setCoUsuario(Integer coUsuario) {
        this.coUsuario = coUsuario;
    }

    public String getNoUsuario() {
        return noUsuario;
    }

    public void setNoUsuario(String noUsuario) {
        this.noUsuario = noUsuario;
    }

    public String getTxPassword() {
        return txPassword;
    }

    public void setTxPassword(String txPassword) {
        this.txPassword = txPassword;
    }

    public String getFlActivo() {
        return flActivo;
    }

    public void setFlActivo(String flActivo) {
        this.flActivo = flActivo;
    }
}
