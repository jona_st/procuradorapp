package pe.edu.upc.procuradorapp.rest;

import java.util.List;

import pe.edu.upc.procuradorapp.beans.SiniestroBean;
import pe.edu.upc.procuradorapp.beans.UserBean;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RestApi {

    @POST("/ProcuradorRest/webresources/Usuario/login")
    Call<UserBean> login(@Body UserBean userBean);
    @POST("/ProcuradorRest/webresources/Siniestro/bandeja")
    Call<List<SiniestroBean>> bandeja(@Body UserBean userBean);
    @POST("/ProcuradorRest/webresources/Siniestro/cola")
    Call<List<SiniestroBean>> colas();

    @POST("/ProcuradorRest/webresources/Siniestro/tomarSiniestro")
    Call<Integer>  tomarSiniestro(@Body SiniestroBean siniestroBean);
}
