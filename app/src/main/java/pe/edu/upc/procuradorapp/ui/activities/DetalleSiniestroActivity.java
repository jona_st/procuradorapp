package pe.edu.upc.procuradorapp.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import pe.edu.upc.procuradorapp.R;
import pe.edu.upc.procuradorapp.beans.SiniestroBean;
import pe.edu.upc.procuradorapp.ui.fragments.BandejaSiniestrosFragment;
import pe.edu.upc.procuradorapp.ui.fragments.ColaSiniestrosFragment;
import pe.edu.upc.procuradorapp.ui.fragments.DataSiniestroFragment;
import pe.edu.upc.procuradorapp.ui.fragments.MapSiniestroFragment;

public class DetalleSiniestroActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, MapSiniestroFragment.OnFragmentInteractionListener,DataSiniestroFragment.OnFragmentInteractionListener{
    private static final String BUNDLE_SINIESTER = "Siniester";
    private SiniestroBean siniestroBean;


    public static Intent getCallingIntent(Context poContext, SiniestroBean siniestroBean) {
        Bundle loBundle = new Bundle();
        loBundle.putSerializable(BUNDLE_SINIESTER, siniestroBean);
        Intent loIntent = new Intent(poContext, DetalleSiniestroActivity.class);
        loIntent.putExtras(loBundle);
        return loIntent;

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_siniestro);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        siniestroBean= (SiniestroBean) getIntent().getSerializableExtra(BUNDLE_SINIESTER);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        addFragment(R.id.rlContent, MapSiniestroFragment.newInstance(siniestroBean));
     //   addFragment(R.id.rlContent, DetalleSiniestro.newInstance(siniestroBean));

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_general, menu);
        return true;
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_general_salir) {
            Intent loginActivity = new Intent(this,LoginActivity.class);
            startActivity(loginActivity);
            finish();
        }


        return super.onOptionsItemSelected(item);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_detail_ubicanos:
                    setTitle("Localización del siniestro");
                    replaceFragment(R.id.rlContent, MapSiniestroFragment.newInstance(siniestroBean));
                    return true;
                case R.id.menu_detail_data:
                    setTitle("Datos del siniestro");
                    replaceFragment(R.id.rlContent, DataSiniestroFragment.newInstance(siniestroBean));
                    return true;


            }
            return false;
        }
    };

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_detail_ubicanos:
                setTitle("Localización del siniestro");
                replaceFragment(R.id.rlContent, MapSiniestroFragment.newInstance(siniestroBean));
                return true;
            case R.id.menu_detail_data:
                setTitle("Datos del siniestro");
                replaceFragment(R.id.rlContent, DataSiniestroFragment.newInstance(siniestroBean));
                return true;

        }
        return false;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    public void addFragment(int containerViewId, Fragment fragment) {
        final FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, fragment);
        fragmentTransaction.commit();
    }

    public void replaceFragment(int containerViewId, Fragment fragment) {
        final FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(containerViewId, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
