package pe.edu.upc.procuradorapp.ui.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

import pe.edu.upc.procuradorapp.Constantes;
import pe.edu.upc.procuradorapp.R;
import pe.edu.upc.procuradorapp.beans.UserBean;
import pe.edu.upc.procuradorapp.rest.RestApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity  {
    private String TAG = "codUsuario";

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };

    // UI references.


    @BindView  (R.id.til_email) TextInputLayout tilEmail;
    @BindView  (R.id.til_clave) TextInputLayout tilClave;
    @BindView  (R.id.tiet_email) TextInputEditText tiet_email;
    @BindView  (R.id.tiet_clave) TextInputEditText tiet_clave;

    @BindView  (R.id.login_progress )View mProgressView;
    @BindView  (R.id.login_form) View mLoginFormView;

    RestApi restApi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        // Set up the login form.
        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
        restApi = Constantes.createRestApi();

    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        tilEmail.setError(null);
        tilClave.setError(null);

        // Store values at the time of the login attempt.
        String email = tiet_email.getText().toString();
        String password = tiet_clave.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            tilClave.setError(getString(R.string.error_field_required));
            focusView = tiet_email;
            cancel = true;
        }else{
            /*if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                tilClave.setError(getString(R.string.error_invalid_password));
                focusView = tiet_clave;
                cancel = true;
            }*/
        }



        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            tilEmail.setError(getString(R.string.error_field_required));
            focusView = tiet_email;
            cancel = true;
        }
        /*else if (!isEmailValid(email)) {
            tilEmail.setError(getString(R.string.error_invalid_email));
            focusView = tiet_email;
            cancel = true;
        }
        */
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            taskLogin(email, password);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public void taskLogin(String email, String password){
        showProgress(true);

        UserBean userBean = new UserBean();
        userBean.setNoUsuario(email);
        userBean.setTxPassword(password);


        Call<UserBean> call = restApi.login(userBean);

        call.enqueue(new Callback<UserBean>() {
            @Override
            public void onResponse(Call<UserBean> call, Response<UserBean> response) {
                switch (response.code()) {
                    case 200:
                        showProgress(false);
                        finish();
                        Intent intent = new Intent(LoginActivity.this,MainActivity.class);

                        Bundle bundle = new Bundle();

                        bundle.putInt(TAG,response.body().getCoUsuario());
                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;
                    default:
                        showProgress(false);
                        tilClave.setError(getString(R.string.error_incorrect_password));
                        tiet_clave.requestFocus();
                        break;
                }
            }

            @Override
            public void onFailure(Call<UserBean> call, Throwable t) {
                showProgress(false);
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                Log.e("error", t.toString());
            }
        });
    }
}

