package pe.edu.upc.procuradorapp.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import pe.edu.upc.procuradorapp.Constantes;
import pe.edu.upc.procuradorapp.R;
import pe.edu.upc.procuradorapp.beans.SiniestroBean;
import pe.edu.upc.procuradorapp.beans.UserBean;
import pe.edu.upc.procuradorapp.rest.RestApi;
import pe.edu.upc.procuradorapp.ui.fragments.BandejaSiniestrosFragment;
import pe.edu.upc.procuradorapp.ui.fragments.ColaSiniestrosFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ColaSiniestrosFragment.OnColaSiniestrosFragmentListener, BandejaSiniestrosFragment.OnBandejaSiniestrosFragmentListener{

    private static final String BUNDLE_SINIESTER = "Siniester";
    private String TAG = "codUsuario";

    private Integer codUsuario;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_main_cola_siniestro:
                    setTitle("Cola de siniestros");
                    replaceFragment(R.id.rlContent,ColaSiniestrosFragment.newInstance(codUsuario));
                    return true;
                case R.id.menu_main_bandeja:
                    setTitle("Bandeja de siniestros");
                    replaceFragment(R.id.rlContent,BandejaSiniestrosFragment.newInstance(codUsuario));




                    return true;

            }
            return false;
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        codUsuario=getIntent().getIntExtra(TAG,1);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        addFragment(R.id.rlContent, ColaSiniestrosFragment.newInstance(codUsuario));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_general, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_general_salir) {
            Intent loginActivity = new Intent(this,LoginActivity.class);
            startActivity(loginActivity);
            finish();
        }


        return super.onOptionsItemSelected(item);
    }

    public void addFragment(int containerViewId, Fragment fragment) {
        final FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, fragment);
        fragmentTransaction.commit();
    }

    public void replaceFragment(int containerViewId, Fragment fragment) {
        final FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(containerViewId, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return true;


    }


    @Override
    public void goToDetailSiniester(SiniestroBean siniestroBean) {
        Intent intent = DetalleSiniestroActivity.getCallingIntent(this, siniestroBean);

        startActivity(intent);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
