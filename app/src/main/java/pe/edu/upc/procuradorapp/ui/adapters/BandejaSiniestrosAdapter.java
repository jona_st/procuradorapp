package pe.edu.upc.procuradorapp.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import pe.edu.upc.procuradorapp.R;
import pe.edu.upc.procuradorapp.beans.SiniestroBean;

public class BandejaSiniestrosAdapter extends RecyclerView.Adapter<BandejaSiniestrosAdapter.BandejaSiniestroViewHolder> {

    public interface OnItemClickListener {
        void onSiniestroItemClicked(SiniestroBean siniestroBean);
    }

    private List<SiniestroBean> siniestroBeanList;
    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;
    private Context context;


    public BandejaSiniestrosAdapter(Context context) {
        this.context = context;

        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        siniestroBeanList = Collections.emptyList();
    }

    @Override
    public BandejaSiniestroViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.row_bandeja_siniestro, parent, false);
        return new BandejaSiniestroViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BandejaSiniestroViewHolder holder, int position) {
        final SiniestroBean siniestroBean = this.siniestroBeanList.get(position);

        holder.txtNumberSiniester.setText(String.valueOf(siniestroBean.getCoSiniestro()));
        holder.txtDateSiniester.setText(siniestroBean.getFeCreacion());
        holder.txtNameSiniester.setText(siniestroBean.getNuDenunciaPolicial());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (BandejaSiniestrosAdapter.this.onItemClickListener != null) {
                    BandejaSiniestrosAdapter.this.onItemClickListener.onSiniestroItemClicked(siniestroBean);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (this.siniestroBeanList != null) ? this.siniestroBeanList.size() : 0;
    }

    public void setSiniestroBeanList(List<SiniestroBean> siniestroBeanList) {
        this.siniestroBeanList = siniestroBeanList;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    static class BandejaSiniestroViewHolder extends RecyclerView.ViewHolder {


        TextView txtNumberSiniester;
        TextView txtDateSiniester;
        TextView txtNameSiniester;


        BandejaSiniestroViewHolder(View view) {
            super(view);
            txtNumberSiniester = view.findViewById(R.id.txtNumberSiniester);
            txtDateSiniester = view.findViewById(R.id.txtDateSinister);
            txtNameSiniester=view.findViewById(R.id.txtNameSiniester);

        }
    }
}
