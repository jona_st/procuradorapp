package pe.edu.upc.procuradorapp.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import pe.edu.upc.procuradorapp.Constantes;
import pe.edu.upc.procuradorapp.R;
import pe.edu.upc.procuradorapp.beans.ProcuradorBean;
import pe.edu.upc.procuradorapp.beans.SiniestroBean;
import pe.edu.upc.procuradorapp.beans.UserBean;
import pe.edu.upc.procuradorapp.rest.RestApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ColaSiniestrosAdapter extends RecyclerView.Adapter<ColaSiniestrosAdapter.ColaSiniestroViewHolder> {

    public interface OnItemClickListener {
        void onSiniestroItemClicked(SiniestroBean siniestroBean);
    }

    private List<SiniestroBean> siniestroBeanList;
    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;
    private Context context;
    private Button bAceptar;

    public Integer getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(Integer codUsuario) {
        this.codUsuario = codUsuario;
    }

    private Integer codUsuario;


    public ColaSiniestrosAdapter(Context context) {
        this.context = context;

        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        siniestroBeanList = Collections.emptyList();
    }

    @Override
    public ColaSiniestroViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.row_siniestro, parent, false);

         bAceptar=view.findViewById(R.id.bAceptar);
        return new ColaSiniestroViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ColaSiniestroViewHolder holder, final int position) {
        final SiniestroBean siniestroBean = this.siniestroBeanList.get(position);

        holder.txtNumberSiniester.setText(String.valueOf(siniestroBean.getCoSiniestro()));
        holder.txtDateSiniester.setText(siniestroBean.getFeCreacion());


        bAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tomarSiniestro(siniestroBean,position);

            }
        });


    }

    private void tomarSiniestro(SiniestroBean siniestroBean,Integer position) {


        RestApi restApi= Constantes.createRestApi();
        ProcuradorBean userBean = new ProcuradorBean();
        userBean.setCoProcurador(codUsuario);
        siniestroBean.setCoProcurador(userBean);

        Call <Integer> call = restApi.tomarSiniestro(siniestroBean);

        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                switch (response.code()) {
                    case 200:


                        break;
                    default:

                        break;
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {

            }
        });
        siniestroBeanList.remove(position);

        setSiniestroBeanList(siniestroBeanList);


    }

    @Override
    public int getItemCount() {
        return (this.siniestroBeanList != null) ? this.siniestroBeanList.size() : 0;
    }

    public void setSiniestroBeanList(List<SiniestroBean> siniestroBeanList) {
        this.siniestroBeanList = siniestroBeanList;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    static class ColaSiniestroViewHolder extends RecyclerView.ViewHolder {


        TextView txtNumberSiniester;
        TextView txtDateSiniester;


        ColaSiniestroViewHolder(View view) {
            super(view);
            txtNumberSiniester = view.findViewById(R.id.txtNumberSiniester);
            txtDateSiniester = view.findViewById(R.id.txtDateSinister);

        }
    }


    public interface SubmitListener {

        void onSubmit();
    }

    private SubmitListener onSubmitListener;

    public void setSubmitListener(SubmitListener onSubmitListener){
        this.onSubmitListener = onSubmitListener;
    }

    public SubmitListener getOnSubmitListener(){
        return onSubmitListener;
    }
}
