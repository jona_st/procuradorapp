package pe.edu.upc.procuradorapp.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import pe.edu.upc.procuradorapp.Constantes;
import pe.edu.upc.procuradorapp.R;
import pe.edu.upc.procuradorapp.beans.SiniestroBean;
import pe.edu.upc.procuradorapp.beans.UserBean;
import pe.edu.upc.procuradorapp.rest.RestApi;
import pe.edu.upc.procuradorapp.ui.adapters.BandejaSiniestrosAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BandejaSiniestrosFragment extends Fragment implements BandejaSiniestrosAdapter.OnItemClickListener {
    private static final String TAG = BandejaSiniestrosFragment.class.getSimpleName();


    RestApi restApi;
    private Integer codUsuario;
    public interface OnBandejaSiniestrosFragmentListener {
        void goToDetailSiniester(SiniestroBean SiniestroBean);
    }

    private OnBandejaSiniestrosFragmentListener onBandejaSiniestrosFragmentListener;
    @BindView(R.id.rcvSBandejaSiniestro)
    RecyclerView rcvBandejaSiniestros;

    private BandejaSiniestrosAdapter siniestrosAdapter;

    public static BandejaSiniestrosFragment newInstance(Integer codUsuario) {

        Bundle bundle = new Bundle();
        bundle.putInt("codUsuario",codUsuario);
        BandejaSiniestrosFragment siniesterFragment = new BandejaSiniestrosFragment();
        siniesterFragment.setArguments(bundle);

        return siniesterFragment;
    }

    public BandejaSiniestrosFragment() {
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnBandejaSiniestrosFragmentListener) {
            this.onBandejaSiniestrosFragmentListener = (OnBandejaSiniestrosFragmentListener) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Bundle data
        restApi = Constantes.createRestApi();
        this.codUsuario=getArguments().getInt("codUsuario");


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_bandeja_siniestro, container, false);
        rcvBandejaSiniestros=view.findViewById(R.id.rcvSBandejaSiniestro);
        setupRecyclerView();


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Acciones en la vista

        loadSiniestros();
    }

    private void setupRecyclerView() {
        siniestrosAdapter =new BandejaSiniestrosAdapter(this.getContext());
        siniestrosAdapter.setOnItemClickListener(this);
        LinearLayoutManager loFrequentPaymentLayoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        this.rcvBandejaSiniestros.setLayoutManager(loFrequentPaymentLayoutManager);
        this.rcvBandejaSiniestros.setAdapter(siniestrosAdapter);

    }

    private void loadSiniestros(){





                UserBean userbean = new UserBean();
                userbean.setCoUsuario(this.codUsuario);



                Call<List<SiniestroBean>> call = restApi.bandeja(userbean);

                call.enqueue(new Callback<List<SiniestroBean>>() {
                    @Override
                    public void onResponse(Call<List<SiniestroBean>> call, Response<List<SiniestroBean>> response) {
                        switch (response.code()) {
                            case 200:


                                List<SiniestroBean> listSiniestroBean = new ArrayList<>();
                                listSiniestroBean = response.body();

                                siniestrosAdapter.setSiniestroBeanList(listSiniestroBean);
                                break;
                            default:

                                break;
                        }
                    }

                    @Override
                    public void onFailure(Call<List<SiniestroBean>> call, Throwable t) {

                        //Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                        Log.e("error", t.toString());
                    }
                });



/*
        LinkedList<SiniestroBean> listSiniestroBean=new LinkedList<SiniestroBean>();
        listSiniestroBean.add(new SiniestroBean(3,"24/07/2018" , "Pedro Lopez"));
        listSiniestroBean.add(new SiniestroBean(4,"24/07/2018","Juana Perez"));
        listSiniestroBean.add(new SiniestroBean(5,"23/07/2018","Carla Hidalgo"));
        listSiniestroBean.add(new SiniestroBean(6,"24/07/2018" , "Pedro Lopez"));
        listSiniestroBean.add(new SiniestroBean(7,"24/07/2018","Juana Perez"));
        listSiniestroBean.add(new SiniestroBean(8,"23/07/2018","Carla Hidalgo"));
        listSiniestroBean.add(new SiniestroBean(9,"24/07/2018" , "Pedro Lopez"));
        listSiniestroBean.add(new SiniestroBean(10,"24/07/2018","Juana Perez"));
        listSiniestroBean.add(new SiniestroBean(11,"23/07/2018","Carla Hidalgo"));
        listSiniestroBean.add(new SiniestroBean(3,"24/07/2018" , "Pedro Lopez"));
        listSiniestroBean.add(new SiniestroBean(4,"24/07/2018","Juana Perez"));
        listSiniestroBean.add(new SiniestroBean(5,"23/07/2018","Carla Hidalgo"));
        listSiniestroBean.add(new SiniestroBean(6,"24/07/2018" , "Pedro Lopez"));
        listSiniestroBean.add(new SiniestroBean(7,"24/07/2018","Juana Perez"));
        listSiniestroBean.add(new SiniestroBean(8,"23/07/2018","Carla Hidalgo"));
        listSiniestroBean.add(new SiniestroBean(9,"24/07/2018" , "Pedro Lopez"));
        listSiniestroBean.add(new SiniestroBean(10,"24/07/2018","Juana Perez"));
        listSiniestroBean.add(new SiniestroBean(11,"23/07/2018","Carla Hidalgo"));
        listSiniestroBean.add(new SiniestroBean(3,"24/07/2018" , "Pedro Lopez"));
        listSiniestroBean.add(new SiniestroBean(4,"24/07/2018","Juana Perez"));
        listSiniestroBean.add(new SiniestroBean(5,"23/07/2018","Carla Hidalgo"));
        listSiniestroBean.add(new SiniestroBean(6,"24/07/2018" , "Pedro Lopez"));
        listSiniestroBean.add(new SiniestroBean(7,"24/07/2018","Juana Perez"));
        listSiniestroBean.add(new SiniestroBean(8,"23/07/2018","Carla Hidalgo"));
        listSiniestroBean.add(new SiniestroBean(9,"24/07/2018" , "Pedro Lopez"));
        listSiniestroBean.add(new SiniestroBean(10,"24/07/2018","Juana Perez"));
        listSiniestroBean.add(new SiniestroBean(11,"23/07/2018","Carla Hidalgo"));

        */


    }
    @Override
    public void onSiniestroItemClicked(SiniestroBean siniestroBean) {
        if(onBandejaSiniestrosFragmentListener!=null){
            onBandejaSiniestrosFragmentListener.goToDetailSiniester(siniestroBean);
        }
    }
}
