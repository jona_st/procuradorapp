package pe.edu.upc.procuradorapp.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import pe.edu.upc.procuradorapp.Constantes;
import pe.edu.upc.procuradorapp.R;
import pe.edu.upc.procuradorapp.beans.SiniestroBean;
import pe.edu.upc.procuradorapp.beans.UserBean;
import pe.edu.upc.procuradorapp.rest.RestApi;
import pe.edu.upc.procuradorapp.ui.adapters.ColaSiniestrosAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ColaSiniestrosFragment extends Fragment implements ColaSiniestrosAdapter.OnItemClickListener {
    private static final String TAG = ColaSiniestrosFragment.class.getSimpleName();


    private Integer codUsuario;
    public interface OnColaSiniestrosFragmentListener {
        void goToDetailSiniester(SiniestroBean SiniestroBean);
    }

    private OnColaSiniestrosFragmentListener onColaSiniestrosFragmentListener;
    @BindView(R.id.rcvSColaSiniestro)    RecyclerView rcvColaSiniestros;

    private ColaSiniestrosAdapter siniestrosAdapter;

    RestApi restApi;
    public static ColaSiniestrosFragment newInstance(Integer codUsuario) {

        Bundle bundle = new Bundle();
        ColaSiniestrosFragment siniesterFragment = new ColaSiniestrosFragment();

        bundle.putInt("codUsuario",codUsuario);

        siniesterFragment.setArguments(bundle);
        return siniesterFragment;
    }

    public ColaSiniestrosFragment() {
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnColaSiniestrosFragmentListener) {
            this.onColaSiniestrosFragmentListener = (OnColaSiniestrosFragmentListener) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Bundle data
        restApi = Constantes.createRestApi();

        this.codUsuario=getArguments().getInt("codUsuario");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_cola_siniestros, container, false);
        rcvColaSiniestros=view.findViewById(R.id.rcvSColaSiniestro);


        setupRecyclerView();


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Acciones en la vista
        loadSiniestros();
    }

    private void setupRecyclerView() {
        siniestrosAdapter =new ColaSiniestrosAdapter(this.getContext());
        siniestrosAdapter.setOnItemClickListener(this);
        LinearLayoutManager loFrequentPaymentLayoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        this.rcvColaSiniestros.setLayoutManager(loFrequentPaymentLayoutManager);
        this.rcvColaSiniestros.setAdapter(siniestrosAdapter);

    }

    private void loadSiniestros(){



        Call<List<SiniestroBean>> call = restApi.colas();

        call.enqueue(new Callback<List<SiniestroBean>>() {
            @Override
            public void onResponse(Call<List<SiniestroBean>> call, Response<List<SiniestroBean>> response) {
                switch (response.code()) {
                    case 200:


                        List<SiniestroBean> listSiniestroBean = new ArrayList<>();
                        listSiniestroBean = response.body();

                        siniestrosAdapter.setSiniestroBeanList(listSiniestroBean);
                        siniestrosAdapter.setCodUsuario(codUsuario);
                        break;
                    default:

                        break;
                }
            }

            @Override
            public void onFailure(Call<List<SiniestroBean>> call, Throwable t) {

                //Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                Log.e("error", t.toString());
            }
        });



    }
    @Override
    public void onSiniestroItemClicked(SiniestroBean siniestroBean) {
        if(onColaSiniestrosFragmentListener!=null){
            onColaSiniestrosFragmentListener.goToDetailSiniester(siniestroBean);
        }
    }

}
