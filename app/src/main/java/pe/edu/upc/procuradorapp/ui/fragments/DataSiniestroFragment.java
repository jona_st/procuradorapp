package pe.edu.upc.procuradorapp.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pe.edu.upc.procuradorapp.R;
import pe.edu.upc.procuradorapp.beans.SiniestroBean;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DataSiniestroFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DataSiniestroFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DataSiniestroFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private SiniestroBean siniestroBean;


    public DataSiniestroFragment() {
        // Required empty public constructor
    }


    public static DataSiniestroFragment newInstance(SiniestroBean siniestroBean) {
        Bundle bundle = new Bundle();
        DataSiniestroFragment dataSiniestroFragment = new DataSiniestroFragment();
        dataSiniestroFragment.setArguments(bundle);
        dataSiniestroFragment.setSiniestroBean(siniestroBean);
        return dataSiniestroFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_data_siniestro, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public SiniestroBean getSiniestroBean() {
        return siniestroBean;
    }

    public void setSiniestroBean(SiniestroBean siniestroBean) {
        this.siniestroBean = siniestroBean;
    }
}
