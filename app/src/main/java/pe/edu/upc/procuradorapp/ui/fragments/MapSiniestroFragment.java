package pe.edu.upc.procuradorapp.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import pe.edu.upc.procuradorapp.R;
import pe.edu.upc.procuradorapp.beans.SiniestroBean;


public class MapSiniestroFragment extends Fragment  implements OnMapReadyCallback{
    private static final String BUNDLE_SINIESTER = "Siniester";

    private OnFragmentInteractionListener mListener;
    private SiniestroBean siniestroBean;
    MapView mMapView;
    private GoogleMap googleMap;


    private ColaSiniestrosFragment.OnColaSiniestrosFragmentListener onColaSiniestrosFragmentListener;

    public MapSiniestroFragment() {
        // Required empty public constructor
    }

    public static MapSiniestroFragment newInstance(SiniestroBean siniestroBean) {

        Bundle bundle = new Bundle();
        MapSiniestroFragment mapSiniestroFragment = new MapSiniestroFragment();
        mapSiniestroFragment.setArguments(bundle);
        mapSiniestroFragment.setSiniestroBean(siniestroBean);
        return mapSiniestroFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        //Bundle
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_map_siniestro, container, false);

        mMapView= (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(this);



        return view;
    }

    private void setupMapPosition() {

        String cc=siniestroBean.getTxCordenadas();
        String [] latlng=cc.split(",");
        LatLng latlngFinal = new LatLng(new Double(latlng[0]), new Double(latlng[1]));
        googleMap.addMarker(new MarkerOptions().position(latlngFinal).title("Nro Policial:"+siniestroBean.getNuDenunciaPolicial()));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latlngFinal));


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public SiniestroBean getSiniestroBean() {
        return siniestroBean;
    }

    public void setSiniestroBean(SiniestroBean siniestroBean) {
        this.siniestroBean = siniestroBean;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        setupMapPosition();


    }


}
